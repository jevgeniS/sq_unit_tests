package ee.ester.service;


import ee.ester.model.*;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by jevgenisa on 22.11.2015.
 */
public class BookLoanDecisionService{

    Book book;
    Person person;

    public BookLoanDecisionService(Person person, Book book){
        this.person = person;
        this.book = book;
    }

    public boolean couldBorrowBook(){

        if (personNeedsToBeChecked()){
            return (checkPersonCurrentPenalties() &&
                    checkBookAgeRestriction() &&
                    checkLibraryCardIsActive() &&
                    checkPersonBorrowingHistory());
        }
        else return true;

    }

    /**
     * we assume that person could have a list of roles
     * @return
     */
    protected boolean personNeedsToBeChecked() {
        PersonRole[] roles = person.getRoles();
        if (roles==null){
            return true;
        }
        for (PersonRole role : person.getRoles()){
            switch (role){
                case ADMIN:
                    return false;
            }
        }
        return true;
    }


    protected boolean checkPersonBorrowingHistory() {
        List<BookBorrowing> bookBorrowings = getBookBorrows();
        int activeBookBorrowings=0;
        for (BookBorrowing borrowing : bookBorrowings){
            if (borrowing.isActive())
                activeBookBorrowings++;
        }

        if (activeBookBorrowings>100){
            return false;
        }

        return true;
    }

    protected List<BookBorrowing> getBookBorrows(){
        return new BorrowingHistoryService().getBookBorrows(person);
    }

    protected boolean checkLibraryCardIsActive() {
        LibraryCard libraryCard = getLibraryCard();
        if (libraryCard.isActive()){
            return true;
        }
        return false;
    }


    protected LibraryCard getLibraryCard(){
        return new LibraryCardService().getLibraryCard(person);
    }

    /**
     * we assume that book has an age restriction as a movie
     *
     */
    protected boolean checkBookAgeRestriction() {
        int age = person.getAge();
        Integer ageRestriction = book.getAgeRestriction();
        if (ageRestriction != null && ageRestriction >age){
            return false;
        }
        return true;
    }

    protected boolean checkPersonCurrentPenalties() {
        if (penaltySum().compareTo(new BigDecimal(2)) == -1){
            return true;
        }
        return false;
    }

    protected BigDecimal penaltySum(){

        List<Penalty> penalties = getPersonPenalties();
        BigDecimal sum = new BigDecimal(0);
        for (Penalty p : penalties){
            if (isPenaltyActive(p)){
                sum.add(p.getSum());
            }
        }
        return sum;
    }

    protected List<Penalty> getPersonPenalties(){
        return new PenalitiesService().getPersonPenalties(person);
    }

    protected boolean isPenaltyActive(Penalty penalty){
        if (getPenaltyExceedingDate(penalty).getTime()<new Date().getTime()){
            return true;
        }
        return false;
    }

    protected Date getPenaltyExceedingDate(Penalty penalty){
        Calendar cal = Calendar.getInstance();
        cal.setTime(penalty.getCreationDate());
        cal.add(Calendar.YEAR, 1);
        return cal.getTime();
    }

}
