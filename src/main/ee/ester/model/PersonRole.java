package ee.ester.model;

/**
 * Created by jevgenisa on 22.11.2015.
 */
public enum PersonRole {
    ADMIN, REGULAR, GUEST;
}
