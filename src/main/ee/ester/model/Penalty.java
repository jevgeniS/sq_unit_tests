package ee.ester.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jevgenisa on 22.11.2015.
 */
public class Penalty {
    public Date creationDate;
    public BigDecimal sum;

    public Date getCreationDate() {
        return creationDate;
    }

    public BigDecimal getSum() {
        return sum;
    }
}
