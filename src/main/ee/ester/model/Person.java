package ee.ester.model;

/**
 * Created by jevgenisa on 22.11.2015.
 */
public class Person {

    Integer age;
    private PersonRole[] roles;

    public Integer getAge() {
        return age;
    }

    public PersonRole[] getRoles() {
        return roles;
    }
}
