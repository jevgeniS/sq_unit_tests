package ee.ester.service;

/**
 * Created by jevgenisa on 22.11.2015.
 */

import ee.ester.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookLoanDecisionServiceTest {

    @InjectMocks
    @Spy
    BookLoanDecisionService loanService;

    @Mock
    Person person;

    @Mock
    Book book;


    @Mock
    BorrowingHistoryService borrowingHistoryService= new BorrowingHistoryService();

    @Test
    public void person_should_be_checked_if_does_not_have_roles(){
        when(person.getRoles()).thenReturn(null);
        assertTrue(loanService.personNeedsToBeChecked());
    }

    @Test
    public void person_should_be_checked_if_has_only_REGULAR_role(){
        when(person.getRoles()).thenReturn(new PersonRole[]{PersonRole.REGULAR});
        assertTrue(loanService.personNeedsToBeChecked());
    }

    @Test
    public void person_should_not_be_checked_if_has_ADMIN_role(){
        when(person.getRoles()).thenReturn(new PersonRole[]{PersonRole.ADMIN});
        assertFalse(loanService.personNeedsToBeChecked());
    }

    @Test
    public void check_person_borrowing_history_should_return_true_if_no_any_active_borrowings(){
        List<BookBorrowing> bookBorrowings=new ArrayList<>();
        for (int i = 0; i<101; i++){
            BookBorrowing b= new BookBorrowing();
            b.active=false;
            bookBorrowings.add(b);
        }
        when(loanService.getBookBorrows()).thenReturn(bookBorrowings);
        assertTrue(loanService.checkPersonBorrowingHistory());
    }

    @Test
    public void check_person_borrowing_history_should_return_false_if_more_than_100_active_borrowings(){
        List<BookBorrowing> bookBorrowings=new ArrayList<>();
        for (int i = 0; i<101; i++){
            BookBorrowing b= new BookBorrowing();
            b.active=true;
            bookBorrowings.add(b);
        }
        when(loanService.getBookBorrows()).thenReturn(bookBorrowings);
        assertFalse(loanService.checkPersonBorrowingHistory());
    }

    @Test
    public void check_library_card_active_should_return_true(){
        LibraryCard lc = new LibraryCard();
        lc.active=true;
        when(loanService.getLibraryCard()).thenReturn(lc);
        assertTrue(loanService.checkLibraryCardIsActive());
    }

    @Test
    public void check_library_card_active_should_return_false(){
        LibraryCard lc = new LibraryCard();
        lc.active=false;
        when(loanService.getLibraryCard()).thenReturn(lc);
        assertFalse(loanService.checkLibraryCardIsActive());
    }

    @Test
    public void check_book_age_restriction_should_return_false(){
        when(person.getAge()).thenReturn(5);
        when(book.getAgeRestriction()).thenReturn(18);
        assertFalse(loanService.checkBookAgeRestriction());
    }

    @Test
    public void check_book_age_restriction_should_return_true(){
        when(person.getAge()).thenReturn(50);
        when(book.getAgeRestriction()).thenReturn(18);
        assertTrue(loanService.checkBookAgeRestriction());
    }

    @Test
    public void check_book_age_restriction_should_return_true_if_no_book_restriction(){
        when(person.getAge()).thenReturn(50);
        when(book.getAgeRestriction()).thenReturn(null);
        assertTrue(loanService.checkBookAgeRestriction());
    }

    @Test
    public void penalty_sum_should_calculate_active_sums(){
        List<Penalty> penalties = new ArrayList<Penalty>();
        for (int i =0; i<2;i++){
            Penalty p = new Penalty();
            p.creationDate=new Date();
            p.sum=new BigDecimal(0.1);
            penalties.add(p);
        }
        when(loanService.getPersonPenalties()).thenReturn(penalties);

        assertEquals(new BigDecimal(0.2), loanService.penaltySum());
    }

    @Test
    public void check_person_current_penalties_should_return_true_if_penalties_are_less_than_2(){
        BigDecimal sum = new BigDecimal("1.99");
        when(loanService.getPersonPenalties()).thenReturn(new ArrayList<Penalty>());
        when(loanService.penaltySum()).thenReturn(sum);
        assertTrue(loanService.checkPersonCurrentPenalties());
    }

    @Test
    public void is_penalty_active_should_return_true(){
        when(loanService.getPenaltyExceedingDate(any(Penalty.class))).thenReturn(new Date());
        assertTrue(loanService.isPenaltyActive(new Penalty()));
    }

    @Test
    public void is_penalty_active_should_return_false_if_two_years_passed(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.YEAR, -2);
        when(loanService.getPenaltyExceedingDate(any(Penalty.class))).thenReturn(cal.getTime());
        assertTrue(loanService.isPenaltyActive(new Penalty()));
    }

}